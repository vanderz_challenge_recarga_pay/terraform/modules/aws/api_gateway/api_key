resource "aws_api_gateway_api_key" "main" {
  name        = var.api_key_name
  description = var.api_key_description
  enabled     = var.api_key_enabled
}
