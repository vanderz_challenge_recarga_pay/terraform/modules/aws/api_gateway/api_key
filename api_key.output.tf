output "api_key_value" {
    value = aws_api_gateway_api_key.main.value
}

output "api_key_id" {
    value = aws_api_gateway_api_key.main.id
}